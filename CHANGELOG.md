This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

# Changelog for org.gcube.spatial.data.sdi-service

## [v1.4.4] 2021-06-15
Fixes #20759 (https)
Fixes #21579 (human friendly opendap)


## [v1.4.3] 2020-05-15
changed maven repos
updated bom dependency

## [v1.4.2] 2020-05-15

### Fixes

- Integration with gcube distribution (https://support.d4science.org/issues/19612)
