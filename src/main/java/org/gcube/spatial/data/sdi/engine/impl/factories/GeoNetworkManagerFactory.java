package org.gcube.spatial.data.sdi.engine.impl.factories;

import javax.inject.Inject;

import org.gcube.spatial.data.sdi.engine.GeoNetworkManager;
import org.gcube.spatial.data.sdi.engine.RoleManager;
import org.gcube.spatial.data.sdi.engine.impl.GeoNetworkManagerImpl;
import org.glassfish.hk2.api.Factory;

public class GeoNetworkManagerFactory implements Factory<GeoNetworkManager>{

	@Inject
	private RoleManager manager;
	
	@Override
	public void dispose(GeoNetworkManager instance) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public GeoNetworkManager provide() {
		
		return new GeoNetworkManagerImpl(manager);
	}
	
}
