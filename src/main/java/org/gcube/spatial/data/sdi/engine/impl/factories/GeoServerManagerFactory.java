package org.gcube.spatial.data.sdi.engine.impl.factories;

import org.gcube.spatial.data.sdi.engine.GISManager;
import org.gcube.spatial.data.sdi.engine.impl.GISManagerImpl;
import org.glassfish.hk2.api.Factory;

import lombok.Synchronized;

public class GeoServerManagerFactory implements Factory<GISManager>{
	
	@Override
	public void dispose(GISManager instance) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public GISManager provide() {
		return getInstance();
	}
	
	
	private static GISManager instance=null;
	
	@Synchronized
	private static GISManager getInstance() {
		if(instance==null)
			instance=new GISManagerImpl();
		return instance;
	}
}
