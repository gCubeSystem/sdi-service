package org.gcube.spatial.data.sdi.rest;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;

import org.gcube.smartgears.annotations.ManagedBy;
import org.gcube.spatia.data.model.profiles.ApplicationProfile;
import org.gcube.spatial.data.sdi.SDIServiceManager;
import org.gcube.spatial.data.sdi.engine.SDIManager;
import org.gcube.spatial.data.sdi.model.ScopeConfiguration;
import org.gcube.spatial.data.sdi.model.ServiceConstants;
import org.gcube.spatial.data.sdi.model.health.HealthReport;

import lombok.extern.slf4j.Slf4j;

@Path(ServiceConstants.INTERFACE)
//@Api(value=ServiceConstants.INTERFACE)
@ManagedBy(SDIServiceManager.class)
@Slf4j
public class SDI {

	@Inject
	private SDIManager sdiManager;
	
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public ScopeConfiguration getConfiguration(){
		try {
			ScopeConfiguration config=sdiManager.getContextConfiguration();
			log.debug("Served Configuration");
			return config;
		}catch(Throwable t) {
			log.error("Unable to serve get configuration");
			throw new WebApplicationException("Unable to get configuration. Contact administrator.",t);
		}
		
	}

	
	@GET
	@Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
	@Path("status")
	public HealthReport getReport() {
		try{
			return sdiManager.getHealthReport();
		}catch(Throwable t) {
			log.error("Unabel to get Health Report ",t);
			throw new WebApplicationException("Unable to check Health. Contact administrator.",t);
		}
	}

	public static final String SERVICE_CLASS_PARAM="service_class";
	public static final String SERVICE_NAME_PARAM="service_name";
	
	@GET
	@Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
	@Path("profile/{"+SERVICE_CLASS_PARAM+"}/{"+SERVICE_NAME_PARAM+"}")
	public ApplicationProfile getProfile(@PathParam(SERVICE_CLASS_PARAM) String serviceClass,
			@PathParam(SERVICE_NAME_PARAM) String serviceName) {
		try {
			log.debug("Looking for Application Profile [SC :{} SN : {}]",serviceClass,serviceName);
			throw new RuntimeException("Feature not yet available");
		}catch(WebApplicationException e){
			log.warn("Unable to serve request",e);
			throw e;
		}catch(Throwable e){
			log.warn("Unable to serve request",e);
			throw new WebApplicationException("Unable to serve request", e);
		}
	}
	
}
