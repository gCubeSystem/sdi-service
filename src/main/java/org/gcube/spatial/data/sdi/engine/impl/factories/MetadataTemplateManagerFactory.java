package org.gcube.spatial.data.sdi.engine.impl.factories;

import org.gcube.spatial.data.sdi.engine.TemplateManager;
import org.gcube.spatial.data.sdi.engine.impl.metadata.MetadataTemplateManagerImpl;
import org.glassfish.hk2.api.Factory;

import lombok.Synchronized;

public class MetadataTemplateManagerFactory implements Factory<TemplateManager>{
	@Override
	public TemplateManager provide() {
		return getInstance();
	}

	@Override
	public void dispose(TemplateManager instance) {
		// TODO Auto-generated method stub
		
	}
	
	
	private static TemplateManager instance = null;
	
	@Synchronized
	private static final TemplateManager getInstance() {
		if(instance==null) {
			instance=new MetadataTemplateManagerImpl();
			try {
				((MetadataTemplateManagerImpl)instance).defaultInit();
				}catch(Exception e) {
					throw new RuntimeException("Unable to init temp ",e);
				}
		}
		return instance;
	}
}
