package org.gcube.spatial.data.sdi.engine.impl.faults.gn;

public class MetadataNotFoundException extends MetadataException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5964532576083669460L;

	public MetadataNotFoundException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public MetadataNotFoundException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public MetadataNotFoundException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public MetadataNotFoundException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public MetadataNotFoundException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	
	
}
