package org.gcube.spatial.data.sdi;

import java.net.URL;

import javax.ws.rs.ApplicationPath;

import org.aopalliance.reflect.Metadata;
import org.gcube.smartgears.ContextProvider;
import org.gcube.smartgears.configuration.container.ContainerConfiguration;
import org.gcube.smartgears.context.application.ApplicationContext;
import org.gcube.spatial.data.sdi.engine.GISManager;
import org.gcube.spatial.data.sdi.engine.GeoNetworkManager;
import org.gcube.spatial.data.sdi.engine.RoleManager;
import org.gcube.spatial.data.sdi.engine.SDIManager;
import org.gcube.spatial.data.sdi.engine.TemplateManager;
import org.gcube.spatial.data.sdi.engine.TemporaryPersistence;
import org.gcube.spatial.data.sdi.engine.ThreddsManager;
import org.gcube.spatial.data.sdi.engine.impl.factories.GeoNetworkManagerFactory;
import org.gcube.spatial.data.sdi.engine.impl.factories.GeoServerManagerFactory;
import org.gcube.spatial.data.sdi.engine.impl.factories.MetadataTemplateManagerFactory;
import org.gcube.spatial.data.sdi.engine.impl.factories.RoleManagerFactory;
import org.gcube.spatial.data.sdi.engine.impl.factories.SDIManagerFactory;
import org.gcube.spatial.data.sdi.engine.impl.factories.TemporaryPersistenceFactory;
import org.gcube.spatial.data.sdi.engine.impl.factories.ThreddsManagerFactory;
import org.gcube.spatial.data.sdi.model.ServiceConstants;
import org.gcube.spatial.data.sdi.rest.GeoNetwork;
import org.gcube.spatial.data.sdi.rest.GeoServer;
import org.gcube.spatial.data.sdi.rest.SDI;
import org.gcube.spatial.data.sdi.rest.Thredds;
import org.glassfish.hk2.utilities.binding.AbstractBinder;
import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.glassfish.jersey.server.ResourceConfig;

import lombok.extern.slf4j.Slf4j;
@Slf4j
@ApplicationPath(ServiceConstants.APPLICATION)
public class SDIService extends ResourceConfig{

//	@Inject
//	MetadataTemplateManager templateManager;
//	@Inject
//	TemporaryPersistence persistence;
//	
	public SDIService() {
		super();
		log.warn("Initializing App : Properties.. ");
		ApplicationContext context=ContextProvider.get();		
		ContainerConfiguration configuration=context.container().configuration();
		try{
			URL resourceUrl = context.application().getResource("/WEB-INF/config.properties");
			LocalConfiguration.init(resourceUrl).
			setTemplateConfigurationObject(ContextProvider.get());
			
		}catch(Throwable t){
			log.debug("Listing available paths");
			for(Object obj:context.application().getResourcePaths("/WEB-INF"))
				log.debug("OBJ : {} ",obj);
			
			throw new RuntimeException("Unable to load configuration properties",t);
		}
		
		
		
		packages("org.gcube.spatial.data");
		
		
		log.warn("Initializing App : Binders");
		
		AbstractBinder binder = new AbstractBinder() {
			@Override
			protected void configure() {                    
				bindFactory(SDIManagerFactory.class).to(SDIManager.class);
				bindFactory(GeoNetworkManagerFactory.class).to(GeoNetworkManager.class);
				bindFactory(ThreddsManagerFactory.class).to(ThreddsManager.class);
				bindFactory(GeoServerManagerFactory.class).to(GISManager.class);
				bindFactory(MetadataTemplateManagerFactory.class).to(TemplateManager.class);
				bindFactory(RoleManagerFactory.class).to(RoleManager.class);
				bindFactory(TemporaryPersistenceFactory.class).to(TemporaryPersistence.class);
				
			}
		};
		register(binder);
		
		

        register(MultiPartFeature.class);
        registerClasses(SDI.class);
        registerClasses(GeoNetwork.class);
        registerClasses(GeoServer.class);
        registerClasses(Thredds.class);
        registerClasses(Metadata.class);
        
        log.warn("Initialization complete");        
//		register(MoxyXmlFeature.class);
        
		

//		String hostName=configuration.hostname();
//		Integer port=configuration.port();
		
		
		
		
		
		
		//SWAGGER
//		BeanConfig beanConfig = new BeanConfig();
//        beanConfig.setVersion("1.0.0");
//        beanConfig.setSchemes(new String[]{"http","https"});
//        beanConfig.setHost(hostName+":"+port);
//        beanConfig.setBasePath("/gcube/service/");
//        beanConfig.setResourcePackage(GeoNetwork.class.getPackage().getName());
//        beanConfig.setTitle("SDI Service");
//        beanConfig.setDescription("REST Interface towards SDI facilities");
//        beanConfig.setPrettyPrint(true);
//        beanConfig.setScan(true);
//		
//        System.out.println("********************** SDI INIT *****************************");
		
		
        
//		
//		log.debug("Initializing persistence manager.. {} :",persistence);
//		
//		try {
//			persistence.init();
//		} catch (Throwable t) {
//			throw new RuntimeException("Unabel to init persistence. ",t);
//		}
//		log.debug("Initializing template manager.. {} : ",templateManager);
//		
//		ApplicationContext ctx = ContextProvider.get();
//		templateManager.init(ctx);
//        
	}
	
	
	
	
}
