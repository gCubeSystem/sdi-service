package org.gcube.spatial.data.sdi.engine;

import org.gcube.spatial.data.sdi.model.service.GeoServerDescriptor;
import org.gcube.spatial.data.sdi.model.services.GeoServerDefinition;

public interface GISManager extends GeoServiceManager<GeoServerDescriptor, GeoServerDefinition>{

//	public List<GeoServerDescriptor> getConfiguration() throws ConfigurationNotFoundException;
//	public ServiceHealthReport getHealthReport();
//	public String registerService(GeoServerDefinition definition)throws ServiceRegistrationException;
//	String importHostFromToken(String sourceToken, String hostname) throws ServiceRegistrationException;
}
