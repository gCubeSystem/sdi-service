package org.gcube.spatial.data.sdi.engine.impl.factories;

import javax.inject.Inject;

import org.gcube.spatial.data.sdi.engine.TemplateManager;
import org.gcube.spatial.data.sdi.engine.ThreddsManager;
import org.gcube.spatial.data.sdi.engine.impl.ThreddsManagerImpl;
import org.glassfish.hk2.api.Factory;

import lombok.Synchronized;

public class ThreddsManagerFactory implements Factory<ThreddsManager>{

	@Inject
	private TemplateManager manager;
	
	
	@Override
	public ThreddsManager provide() {
		return getInstance(manager);
	}

	@Override
	public void dispose(ThreddsManager instance) {
		// TODO Auto-generated method stub
		
	}
	
	private static ThreddsManager instance=null;
	
	@Synchronized
	private static ThreddsManager getInstance(TemplateManager manager) {
		if(instance==null)
			instance=new ThreddsManagerImpl(manager);
		return instance;
	}
}
