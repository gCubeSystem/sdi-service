package org.gcube.spatial.data.sdi.engine.impl.factories;

import org.gcube.spatial.data.sdi.engine.TemporaryPersistence;
import org.gcube.spatial.data.sdi.engine.impl.TemporaryPersistenceImpl;
import org.glassfish.hk2.api.Factory;

import lombok.Synchronized;

public class TemporaryPersistenceFactory implements Factory<TemporaryPersistence>{

	
	@Override
	public void dispose(TemporaryPersistence arg0) {
		arg0.shutdown();
	}
	
	@Override
	public TemporaryPersistence provide() {
		return getInstance();
	}
	
	
	private static TemporaryPersistence temp=null;
	
	@Synchronized
	private static TemporaryPersistence getInstance(){
		if(temp==null) {
			temp=new TemporaryPersistenceImpl();
			try {
			temp.init();
			}catch(Exception e) {
				throw new RuntimeException("Unable to init temp ",e);
			}
		}
		return temp;
	}
}
